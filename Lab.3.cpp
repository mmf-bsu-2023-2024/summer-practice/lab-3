#include <iostream>

using namespace std;

double leftRectangle(double, double, int);
double sinExp(double);

int main(void)
{
	double beginSegment;
	double endSegment;
	double previosIntegral = 0;
	double nextIntegral = 1;
	double epsilon;
	int n;

	while (true)
	{
		cout << endl << "Enter the value of regs of segment, accuracyand partitions number : ";
		cin >> beginSegment >> endSegment >> epsilon >> n;
		if ((beginSegment < endSegment) && (n > 0) && (epsilon < 1) && (epsilon > 0))
		{
			break;
		}
		cout << endl << "Parameters are incorrect. Try again." << endl;
	}

	system("cls");
	while (abs(previosIntegral - nextIntegral) > epsilon)
	{
		previosIntegral = leftRectangle(beginSegment,
			endSegment, n);
		nextIntegral = leftRectangle(beginSegment,
			endSegment, 2 * n);
		n = 2 * n;
	}

	cout << endl << "The value of integral of function sin(x) * exp(x) on segment ["
		<< beginSegment << ";" << endSegment << "] is equal " << nextIntegral << endl;

	system("pause");
	return 0;

}

double leftRectangle(double lowerLimit, double	upperLimit, int k)
{
	double step = (lowerLimit + upperLimit) / k;
	double integral = 0;
	double t = lowerLimit;

	while (t < upperLimit)
	{
		integral = integral + sinExp(t);
		t = t + step;
	}

	integral = step * integral;
	return integral;
}

double sinExp(double x)
{
	return sin(x) * exp(x);
}
